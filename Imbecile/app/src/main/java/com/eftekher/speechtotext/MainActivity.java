package com.eftekher.speechtotext;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

//import com.example.android.speechtotext.R;

public class MainActivity extends AppCompatActivity implements  TextToSpeech.OnInitListener{

	private TextView txvResult;
	TextToSpeech t1;
    String toSpeak,toMessage,toCom,toStart,number1;
	private TTSListener listener = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		txvResult = (TextView) findViewById(R.id.txvResult);
		t1 = new TextToSpeech(this, this, "com.google.android.tts");
		/*try {
			t1.speak("I am ready for your command ", TextToSpeech.QUEUE_FLUSH, null);
		}
		catch (NullPointerException e)
		{

		}*/
       // getstartInput();

	}

	public void getstartInput() {
		//t1.speak("please give me a contact name", TextToSpeech.QUEUE_FLUSH, null);
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

		if (intent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(intent, 4);

		} else {
			Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
		}
	}

    public void getmesssageInput() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 2);

        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }
	public void getCommandInput() {

		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

		if (intent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(intent, 3);

		} else {
			Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
		}
	}

	public void sendSMS()
    {
        SmsManager sm = SmsManager.getDefault();
        String number = number1;
        String msg = toMessage;
        sm.sendTextMessage(number, null, msg, null, null);
    }

	/*public void homeI()
	{
		Intent homeIntent = new Intent(Intent.ACTION_MAIN);
		homeIntent.addCategory( Intent.CATEGORY_HOME );
		homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(homeIntent);
	}*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {

			case 1:
				if (resultCode == RESULT_OK && data != null) {
					ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
					toSpeak = result.get(0);
					txvResult.setText(toSpeak);
					if(toSpeak.equals("exit"))
						System.exit(0);// homeI();
					NumberGetter();
					break;

				}
			case 2:
				if (resultCode == RESULT_OK && data != null) {
					ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
					toMessage = result.get(0);
					txvResult.setText(toMessage);
					// txvResult.setText(toSpeak);
					if(toMessage.equals("exit"))
						System.exit(0);// homeI();
					t1.speak("should I send now? ", TextToSpeech.QUEUE_FLUSH, null, "1236");
					if(listener != null) {
						listener = null;
					}
					listener = new TTSListener() {
						@Override
						public void onSpeakingDone() {
							getCommandInput();
						}
					};
					//getCommandInput();
				//	Toast.makeText(getApplicationContext(),toMessage, Toast.LENGTH_SHORT).show();
					break;


				}
			case 3:
				if (resultCode == RESULT_OK && data != null) {
					ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
					toCom = result.get(0);
					txvResult.setText(toCom);
					//txvResult.setText(toMessage);
					// txvResult.setText(toSpeak);
					if (toCom.equals("exit"))
						System.exit(0);// homeI();

					if (toCom.equals("yes")) {
						sendSMS();
						t1.speak("message send successful ", TextToSpeech.QUEUE_FLUSH, null, "1237");
						Toast.makeText(getApplicationContext(), toCom, Toast.LENGTH_SHORT).show();
						if (listener != null) {
							listener = null;
						}
						listener = new TTSListener() {
							@Override
							public void onSpeakingDone() {
								System.exit(0);
							}
						};

					} else {
						t1.speak("message send failed", TextToSpeech.QUEUE_FLUSH, null, "1238");
						Toast.makeText(getApplicationContext(), "no", Toast.LENGTH_SHORT).show();
						if (listener != null) {
							listener = null;
						}
						listener = new TTSListener() {
							@Override
							public void onSpeakingDone() {
								System.exit(0);
							}
						};


					}
					break;
				}
            case 4:
                if (resultCode == RESULT_OK && data != null) {
					ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
					toStart = result.get(0);
					txvResult.setText(toStart);
					if (toStart.equals("exit"))
						System.exit(0);// homeI();
					/*if (toStart.equals(" ")) {
						t1.speak("I am waiting for your command, sir", TextToSpeech.QUEUE_FLUSH, null, "1335");
						if (listener != null) {
							listener = null;
						}
						listener = new TTSListener() {
							@Override
							public void onSpeakingDone() {
								getstartInput();
							}
						};
					}*/
                   if(toStart.equals("send SMS")) {
					  // t1.speak("please provide a contact number", TextToSpeech.QUEUE_FLUSH,null );
					   t1.speak("please provide a contact name", TextToSpeech.QUEUE_FLUSH,null, "1235");
					   if(listener != null) {
						   listener = null;
					   }
					   listener = new TTSListener() {
						   @Override
						   public void onSpeakingDone() {
							   getSpeechInput();
						   }
					   };
					   /*getSpeechInput();*/
				   }
				   else
				   {
					   t1.speak("I am unable to perform that command,to send a message please command send SMS?", TextToSpeech.QUEUE_FLUSH,null, "1290");
					  // Toast.makeText(getApplicationContext(),toStart,Toast.LENGTH_SHORT).show();
					   if(listener != null) {
						   listener = null;
					   }
					   listener = new TTSListener() {
						   @Override
						   public void onSpeakingDone() {
							   getstartInput();
						   }
					   };

				   }
                    break;

                }
				break;
		}

	}

	public String getPhoneNumber(String name,Context context)

	{
		String number="";


		Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
				ContactsContract.CommonDataKinds.Phone.NUMBER};

		Cursor people = context.getContentResolver().query(uri, projection, null, null, null);

		int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
		int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

		people.moveToFirst();
		do {

			String Name   = people.getString(indexName);
			String Number = people.getString(indexNumber);
			if(Name.equalsIgnoreCase(name)){return Number.replace("-", "");}
			// Do work...
		} while (people.moveToNext());


		if(!number.equalsIgnoreCase("")){return number.replace("-", "");}
		else return number;
	}



	public void getSpeechInput() {

		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

		if (intent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(intent, 1);

		} else {
			Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
		}
	}

	//@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	//	super.onActivityResult(requestCode, resultCode, data);

	//	switch (requestCode) {
		//	case 1:
			//	if (resultCode == RESULT_OK && data != null) {
			//		ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
             //       toSpeak = result.get(0);
			//		txvResult.setText(toSpeak);
			//	}


		public void NumberGetter() {
			//String text = toSpeak.toString();

			Toast.makeText(getApplicationContext(), toSpeak, Toast.LENGTH_SHORT).show();
			//t1.speak(text, TextToSpeech.QUEUE_FLUSH, null);

			number1 = getPhoneNumber(toSpeak, MainActivity.this);
			if (number1 == "") {
				//Toast.makeText(getApplicationContext(), number1,Toast.LENGTH_SHORT).show();
				t1.speak("number not found", TextToSpeech.QUEUE_FLUSH, null, "1239");
			} else {
				Toast.makeText(getApplicationContext(), number1, Toast.LENGTH_SHORT).show();
				//t1.speak(number1, TextToSpeech.QUEUE_FLUSH, null);

				t1.speak("I am ready for message", TextToSpeech.QUEUE_FLUSH, null, "1240");
				if(listener != null) {
					listener = null;
				}
				listener = new TTSListener() {
					@Override
					public void onSpeakingDone() {
						getmesssageInput();
					}
				};
				//getmesssageInput();

			}
					/* public void onPause(){
                if(t1 !=null){
                    t1.stop();
                    t1.shutdown();
                }
                super.onPause();
            }*/


		}

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            Log.d("ReataTTS", "status success");

			t1.speak("I am ready for your command ", TextToSpeech.QUEUE_FLUSH, null, "1233");
			if(listener != null) {
				listener = null;
			}
			listener = new TTSListener() {
				@Override
				public void onSpeakingDone() {
					getstartInput();
				}
			};




            if (Build.VERSION.SDK_INT >= 15) {
                int ret = t1.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override
                    public void onStart(String utteranceID) {
                        Log.d("ReataTTS", "onStart" + utteranceID);
                    }

                    @Override
                    public void onDone(String utteranceID) {
                        Log.d("ReataTTS", "onDone" + utteranceID);
						listener.onSpeakingDone();
                    }

                    @Override
                    public void onError(String utteranceID) {
                        Log.d("ReataTTS", "onError" + utteranceID);
                    }

                    @Override
                    public void onStop(String utteranceId, boolean interrupted) {
                        Log.d("ReataTTS", "onStop" + utteranceId);
                        super.onStop(utteranceId, interrupted);
                    }
                });

                if (ret != TextToSpeech.SUCCESS) {
                    Log.e("ReataTextToSpeech", "Failed to add utterance progess listener");
                }
            } else {
                int ret = t1.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {
                    @Override
                    public void onUtteranceCompleted(String utteranceID) {
                        Log.d("ReataTTS", "onDone" + utteranceID);
                    }
                });

                if (ret != TextToSpeech.SUCCESS) {
                    Log.e("ReataTextToSpeech", "Failed to add utterance completed listener");
                }
            }

        } else
            Log.e("ReataTextToSpeech", "Initilization Failed!");
    }
    //}
//	}


}
