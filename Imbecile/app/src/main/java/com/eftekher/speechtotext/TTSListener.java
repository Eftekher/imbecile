package com.eftekher.speechtotext;

/**
 * Created by Eftekher on 11/28/2017.
 */

public interface TTSListener {
    public void onSpeakingDone();
}
