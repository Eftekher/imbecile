package com.sriyanksiddhartha.speechtotext;

/**
 * Created by Sarowar on 12/5/2016.
 */
public class ReataTTSSpeakData {

    private String textToSpeak;
    private String ttsGender;
    private String utteranceID;
    private String languageName;

    public ReataTTSSpeakData() {
    }

    public ReataTTSSpeakData(String textToSpeak, String ttsGender, String languageName, String
            utteranceID) {
        this.textToSpeak = textToSpeak;
        this.ttsGender = ttsGender;
        this.utteranceID = utteranceID;
        this.languageName = languageName;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public String getTextToSpeak() {
        return textToSpeak;
    }

    public void setTextToSpeak(String textToSpeak) {
        this.textToSpeak = textToSpeak;
    }

    public String getTtsGender() {
        return ttsGender;
    }

    public void setTtsGender(String ttsGender) {
        this.ttsGender = ttsGender;
    }

    public String getUtteranceID() {
        return utteranceID;
    }

    public void setUtteranceID(String utteranceID) {
        this.utteranceID = utteranceID;
    }
}
