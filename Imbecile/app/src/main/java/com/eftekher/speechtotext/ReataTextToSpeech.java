package com.sriyanksiddhartha.speechtotext;

import java.util.Observable;

/**
 * Created by hasibur on 9/30/16.
 */

public final class ReataTextToSpeech extends Observable/* implements TextToSpeech.OnInitListener*/ {
   /* private final String TAG = "ReataTextToSpeech";
    private Context context;
    private static ReataTextToSpeech INSTANCE;
    private TextToSpeech tts;
    private ReataSpeechRecognizer speechRecognizer;
    private int stringID = 0;

    private Voice ttsVoice = null;
    private HashMap<String, String> params = new HashMap<String, String>();
    private Map<String, TTSListener> callbackMap;
    private Handler mHandler;

    public ReataTextToSpeech(Context context, ReataSpeechRecognizer speechRecognizer, Handler mHandler) {
        this.context = context;
        INSTANCE = this;
        tts = new TextToSpeech(context, this, "com.google.android.tts");
        callbackMap = new HashMap<>();
        this.speechRecognizer = speechRecognizer;
        this.mHandler = mHandler;
    }

    public void setRecognitionEngine(ReataSpeechRecognizer speechRecognizer) {
        Log.d(TAG, "setRecognitionEngine");
        this.speechRecognizer = speechRecognizer;
    }

    public void destroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
            Log.d("ReataTTS", "destroy");
        }
    }

    public void stop() {
        Log.d("ReataTTS", "stop");
        if (tts != null) {
            tts.stop();
        }
    }

    public void speak(ReataTTSSpeakData _TTSSpeakData, TTSListener
            ttsListener) {
        if (_TTSSpeakData != null) {
            Locale ttsLanguage = getLanguage(_TTSSpeakData);
            tts.setLanguage(ttsLanguage);

            if (Build.VERSION.SDK_INT >= 21) {
                try {
                    Voice ttsVoiceObj = getTTSVoice(_TTSSpeakData, ttsLanguage);
                    if (ttsVoiceObj != null) {
                        tts.setVoice(ttsVoiceObj);
                    }
                    ttsVoice = ttsVoiceObj;
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, e.getLocalizedMessage());
                }
            }

            Log.d("ReataTextToSpeech", "speak = \"" + _TTSSpeakData.getTextToSpeak() + "\"");
            String text = _TTSSpeakData.getTextToSpeak();
            if (text == null) {
                text = "  ";
            }
            if (text.length() == 0) {
                text = "   ";
            }
            showMessageToLiveFeed(text);
            callbackMap.put(_TTSSpeakData.getUtteranceID(), ttsListener);
            params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, _TTSSpeakData.getUtteranceID());
            if (Build.VERSION.SDK_INT >= 21) {
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, _TTSSpeakData.getUtteranceID());
            } else {
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, params);
            }
        }
    }

    private Voice getTTSVoice(ReataTTSSpeakData _ttsSpeakData, Locale _ttsLanguage) {
        Voice voice = null;
        if (Build.VERSION.SDK_INT >= 21) {
            String requestedTTSGender = _ttsSpeakData.getTtsGender();
            String _voiceName = null;

            if (requestedTTSGender == null) {
                requestedTTSGender = ReataSettings.getInstance().getGeneralSettingsInstance().getGender();
            }

            if (_ttsLanguage.getDisplayLanguage().equals(Locale.US.getDisplayLanguage())) {
                if (requestedTTSGender.equals(ReataConstants.GENDER_MALE)) {
                    _voiceName = ReataConstants.VOICE_MALE_US;
                } else if (requestedTTSGender.equals(ReataConstants.GENDER_FEMALE)) {
                    _voiceName = ReataConstants.VOICE_FEMALE_US;
                }
            } else if (_ttsLanguage.getDisplayLanguage().equals(Locale.FRENCH.getDisplayLanguage())) {
                if (requestedTTSGender.equals(ReataConstants.GENDER_MALE)) {
                    _voiceName = ReataConstants.VOICE_MALE_FR;
                } else if (requestedTTSGender.equals(ReataConstants.GENDER_FEMALE)) {
                    _voiceName = ReataConstants.VOICE_FEMALE_FR;
                }
            } else if (_ttsLanguage.getDisplayLanguage().equals(new Locale("es", "ES").getDisplayLanguage())) {
                if (requestedTTSGender.equals(ReataConstants.GENDER_MALE)) {
                    _voiceName = ReataConstants.VOICE_MALE_SPA;
                } else if (requestedTTSGender.equals(ReataConstants.GENDER_FEMALE)) {
                    _voiceName = ReataConstants.VOICE_FEMALE_SPA;
                }
            } else if (_ttsLanguage.getDisplayLanguage().equals(Locale.ITALIAN.getDisplayLanguage())) {
                if (requestedTTSGender.equals(ReataConstants.GENDER_MALE)) {
                    _voiceName = ReataConstants.VOICE_MALE_ITA;
                } else if (requestedTTSGender.equals(ReataConstants.GENDER_FEMALE)) {
                    _voiceName = ReataConstants.VOICE_FEMALE_ITA;
                }
            } else if (_ttsLanguage.getDisplayLanguage().equals(Locale.GERMAN.getDisplayLanguage())) {
                if (requestedTTSGender.equals(ReataConstants.GENDER_MALE)) {
                    _voiceName = ReataConstants.VOICE_MALE_GER;
                } else if (requestedTTSGender.equals(ReataConstants.GENDER_FEMALE)) {
                    _voiceName = ReataConstants.VOICE_FEMALE_GER;
                }
            } else if (_ttsLanguage.getDisplayLanguage().equals(new Locale("ar", "AR").getDisplayLanguage())) {
                _voiceName = null;
            }

            if (_voiceName == null) {
                return voice;
            }

            if (ttsVoice != null) {
                if (ttsVoice.getName().equals(_voiceName)) {
                    return ttsVoice;
                }
            }
            for (Voice tmpVoice : tts.getVoices()) {
                if (tmpVoice.getName().equals(_voiceName)) {
                    voice = tmpVoice;
                    break;
                }
            }

            if (voice != null) {
                Log.e("ReataTTS", "Voice set : " + voice.getName());
            }
        }

        return voice;
    }

    private Locale getLanguage(ReataTTSSpeakData _TTSSpeakData) {
        String languageName = _TTSSpeakData.getLanguageName();
        if (languageName == null) {
            languageName = ReataSettings.getInstance().getGeneralSettingsInstance().getLanguage();

        }
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                if (tts.getVoice()!=null &&
                        languageName.equals(tts.getVoice().getLocale().getDisplayLanguage())) {
                    return tts.getVoice().getLocale();
                }
            } catch (IllegalArgumentException e) {
                if (languageName.equals(tts.getLanguage().getDisplayLanguage())) {
                    return tts.getLanguage();
                }
            }
        } else {
            if (languageName.equals(tts.getLanguage().getDisplayLanguage())) {
                return tts.getLanguage();
            }
        }
        Locale locale = Locale.US;

        String languageCode = ReataConfigurableLanguage.Instance().getLanguageCode(languageName);
        if(!languageCode.equals("")) {
            return new Locale(languageCode);
        }

        return locale;
    }

    private void showMessageToLiveFeed(final String text) {
        if (text.length() > 0) {
            speechRecognizer.sendToUiLogger(text, ReataMessengerToUI.MESSAGE_TYPE_FEEDBACK);
        }
    }

    public void speak(String text) {
        ReataTTSSpeakData speakData = new ReataTTSSpeakData(text, null, null, "stringID" +
                (stringID++));
        speak(speakData, null);
    }

    public void speak(String text, String utteranceID) {
        ReataTTSSpeakData speakData = new ReataTTSSpeakData(text, null, null, utteranceID);
        speak(speakData, null);
    }

    public static ReataTextToSpeech getInstance() {
        return INSTANCE;
    }

    public boolean isTtsSpeaking() {
        return tts.isSpeaking();
    }


    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            Log.d("ReataTTS", "status success");

            Locale ttsLanguage = getLanguage(new ReataTTSSpeakData());
            if (tts.isLanguageAvailable(ttsLanguage) >= 0)
                tts.setLanguage(ttsLanguage);

            if (Build.VERSION.SDK_INT >= 21) {
                try {
                    Voice ttsVoice = getTTSVoice(new ReataTTSSpeakData(), ttsLanguage);
                    if (ttsVoice != null) {
                        tts.setVoice(ttsVoice);
                    }
                } catch (IllegalArgumentException e) {
                    Log.e(TAG, e.getLocalizedMessage());
                }
            }

            //utterence completed listener
            if (Build.VERSION.SDK_INT >= 15) {
                int ret = tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override
                    public void onStart(String utteranceID) {
                        Log.d("ReataTTS", "onStart" + utteranceID);
                        speechRecognizer.unMuteReata();
                        onStartSpeaking(utteranceID);
                    }

                    @Override
                    public void onDone(String utteranceID) {
                        Log.d("ReataTTS", "onDone" + utteranceID);
                        onFinishSpeaking(utteranceID);
                    }

                    @Override
                    public void onError(String utteranceID) {
                        Log.d("ReataTTS", "onError" + utteranceID);
                        onErrorSpeaking(utteranceID);
                    }

                    @Override
                    public void onStop(String utteranceId, boolean interrupted) {
                        Log.d("ReataTTS", "onStop" + utteranceId);
                        onStopSpeaking(utteranceId);
                        super.onStop(utteranceId, interrupted);
                    }
                });

                if (ret != TextToSpeech.SUCCESS) {
                    Log.e("ReataTextToSpeech", "Failed to add utterance progess listener");
                }
            } else {
                int ret = tts.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {
                    @Override
                    public void onUtteranceCompleted(String utteranceID) {
                        onFinishSpeaking(utteranceID);
                    }
                });

                if (ret != TextToSpeech.SUCCESS) {
                    Log.e("ReataTextToSpeech", "Failed to add utterance completed listener");
                }
            }

        } else
            Log.e("ReataTextToSpeech", "Initilization Failed!");
    }

    private void onStartSpeaking(String utteranceID) {
        TTSListener ttsListener = callbackMap.get(utteranceID);
        if (ttsListener != null) {
            ttsListener.onStartSpeaking(utteranceID);
        }
    }

    private void onFinishSpeaking(String utteranceID) {
        TTSListener ttsListener = callbackMap.get(utteranceID);
        if (ttsListener != null) {
            ttsListener.onFinishedSpeaking(utteranceID);
        }
    }

    private void onErrorSpeaking(String utteranceID) {
        TTSListener ttsListener = callbackMap.get(utteranceID);
        if (ttsListener != null) {
            ttsListener.onErrorSpeaking(utteranceID);
        }
    }

    private void onStopSpeaking(String utteranceID) {
        TTSListener ttsListener = callbackMap.get(utteranceID);
        if (ttsListener != null) {
            ttsListener.onStopSpeaking(utteranceID);
        }
    }

    private void onPauseSpeaking(String utteranceID) {
        TTSListener ttsListener = callbackMap.get(utteranceID);
        if (ttsListener != null) {
            ttsListener.onPauseSpeaking(utteranceID);
        }
    }

    public void setFaster() {
        //TODO
        //what is speech rate? set rate carefully
        //35% faster
        tts.setSpeechRate(2f);
    }

    public void setSlower() {
        //TODO
        //what is speech rate? set rate carefully
        //35% slower
        tts.setSpeechRate(0.5f);
    }

    public void setDefaultRate() {
        tts.setSpeechRate(1f);
    }
*/

}
